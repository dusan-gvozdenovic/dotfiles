nnoremap <silent> <F2> :NERDTreeToggle<CR>
nnoremap <silent> <F3> :CocCommand clangd.switchSourceHeader<CR>
nnoremap <silent> <F12><F12> :qa!<CR>
nnoremap <silent> <S-Right> :vertical resize +8<CR>
nnoremap <silent> <S-Left> :vertical resize -8<CR>
nnoremap <silent> <S-Down> :resize +4<CR>
nnoremap <silent> <S-Up> :resize -4<CR>
nnoremap <silent> <C-.> :CocAction<CR>
nnoremap <silent> <C-_> <Leader>c<Space>
nnoremap <silent> <C-/> <Leader>c<Space>
nnoremap <silent> <Leader><Leader> :noh<CR>
nnoremap <silent> tc :tabnew<CR>
nnoremap <silent> tn :tabnext<CR>
nnoremap <silent> tp :tabprev<CR>
nnoremap <silent> tq :tabclose<CR>

