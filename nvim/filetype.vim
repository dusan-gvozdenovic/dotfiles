autocmd FileType,BufNewFile,BufRead *lpp set ft=lex
autocmd FileType,BufNewFile,BufRead *ypp set ft=yacc

autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
