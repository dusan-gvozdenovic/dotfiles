set fish_greeting ""
#export LANG=sr_RS.UTF-8
#export TERM=xterm-256color
export EDITOR=vim
export STEAM_FRAME_FORCE_CLOSE=1
export ALSA_CONFIG_PATH=/etc/alsa-pulse.conf
set FLINE_PATH $HOME/.config/fish/fishline
source $FLINE_PATH/fishline.fish
source $FLINE_PATH/themes/default.fish
set PATH $PATH ~/.local/bin/
set PATH $PATH /opt/cuda/bin/
#set PATH $PATH $HOME/.config/composer/vendor/bin/
#set PATH $PATH $HOME/.gem/ruby/2.5.0/bin/
set I3_SCRIPTS_ACCENT_COLOR "#bc7700"

function fish_prompt
    fishline -s $status USERHOST STATUS JOBS PWD GIT WRITE ROOT SPACE
end

#if [ "$TERM" = "linux" ];
#/bin/echo -e "
	#\e]P0002b36
	#\e]P1dc322f
	#\e]P2859900
	#\e]P3b58900
	#\e]P4268bd2
	#\e]P56c71c4
	#\e]P62aa198
	#\e]P793a1a1
	#\e]P8657b83
	#\e]P9dc322f
	#\e]PA859900
	#\e]PBb58900
	#\e]PC268bd2
	#\e]PD6c71c4
	#\e]PE2aa198
	#\e]PFfdf6e3
	#"
	## get rid of artifacts
	#clear
#end
