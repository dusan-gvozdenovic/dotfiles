"colorscheme desert

"if (empty($TMUX))
  "if (has("nvim"))
    ""For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    "let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  "endif
  ""For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  ""Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  "" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  "if (has("termguicolors"))
    "set termguicolors
  "endif
"endif

"colorscheme desert
colorscheme gruvbox

" --------------------------------- Presets ---------------------------------- "

if g:colors_name == "gruvbox"
    if (has("termguicolors"))
        set termguicolors
    endif

    let g:gruvbox_contrast_dark = 'hard'
    let g:gruvbox_bold = 1
    let g:airline_theme = 'gruvbox'
else
    let g:airline_theme = 'papercolor'

    hi Comment ctermfg=243
    highlight Pmenu ctermfg=7 ctermbg=238 guibg=darkgrey
    highlight PmenuSel ctermfg=255 ctermbg=blue guibg=darkgrey
    highlight PmenuSbar ctermfg=0 ctermbg=8 guibg=Grey
    highlight PmenuThumb ctermbg=0 guibg=White
endif

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

set colorcolumn=80

"BUG: Fix this
"set cursorline
hi! LineNr ctermfg=darkgrey
hi! CursorLineNr ctermfg=grey
hi! CursorLine ctermfg=none
hi! SignColumn ctermbg=none
hi! ColorColumn ctermbg=238

highlight ExtraWhitespace ctermbg=white guibg=white
match ExtraWhitespace /\s\+$\| \+\ze\t|[^\t]\zs\t\+/
