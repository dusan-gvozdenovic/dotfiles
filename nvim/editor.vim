filetype plugin on
filetype indent on
syntax on
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set mouse=a

