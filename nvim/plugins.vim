set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim

call vundle#rc('~/.config/nvim/bundle/')
call vundle#begin()

" --------------------------------- General ---------------------------------- "

Plugin 'VundleVim/Vundle.vim'

Plugin 'preservim/nerdtree'

Plugin 'preservim/nerdcommenter'

Plugin 'editorconfig/editorconfig-vim'

Plugin 'tpope/vim-fugitive'

Plugin 'airblade/vim-gitgutter'

" ------------------------------ User Interface ------------------------------ "

Plugin 'morhetz/gruvbox'

Plugin 'vim-airline/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

" -------------------------------- Languages --------------------------------- "

Plugin 'neoclide/coc.nvim', {'branch': 'release'}

Plugin 'HiPhish/guile.vim'

Plugin 'bhurlow/vim-parinfer'

Plugin 'alaviss/nim.nvim'

Plugin 'vim-pandoc/vim-pandoc-syntax'

call vundle#end()
