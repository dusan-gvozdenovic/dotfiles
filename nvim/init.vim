set shell=/bin/bash

source ~/.config/nvim/plugins.vim

source ~/.config/nvim/editor.vim

source ~/.config/nvim/filetype.vim

source ~/.config/nvim/colorscheme.vim

source ~/.config/nvim/keymap.vim

source ~/.config/nvim/coc-init.vim
