SCRIPTS_DIR=$(HOME)/.config/i3-scripts/
CONFIG_DIR=$(HOME)/.config/

install:
	cp .vimrc        $(HOME)
	cp .Xresources   $(HOME)
	cp .startup      $(SCRIPTS_DIR)
	cp -r i3/        $(CONFIG_DIR)
	cp -r i3blocks/  $(CONFIG_DIR)
	cp -r conky/     $(CONFIG_DIR)
	cp -r Code/      $(CONFIG_DIR)
	cp -r fish/      $(CONFIG_DIR)
