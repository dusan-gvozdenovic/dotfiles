set nocompatible
filetype off

set shell=/bin/bash
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'

call vundle#end()

set tabstop=4
set shiftwidth=4
set autoindent
set nu

filetype plugin indent on
syntax on
colorscheme desert

:nmap <C-k> :NERDTreeToggle<CR>
:nmap <C-_> <Leader>c<Space>
:vmap <C-_> <Leader>c<Space>
